const mongoose= require('../database');

const TurmaSchema = new mongoose.Schema({

    id:{
        type:Int32Array,
        require:true,
    },
    nome:{
        type:String,
        require:true,
    },
    Capacidade:{
        type:Int32Array,
        require:true,
    }

});

const Turma = mongoose.model('Turma', TurmaSchema);

