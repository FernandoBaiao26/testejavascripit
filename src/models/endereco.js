const mongoose= require('../database');

const EnderecoSchema = new mongoose.Schema({

    logradouro:{
        type:String,
        require:true,
    },
    complemento:{
        type:String,
        require:true,
    },
    bairro:{
        type:String,
        require:true,
    },
    cidade:{
        type:String,
        require:true,
    },
    estado:{
        type:String,
        require:true,
    }

});

const Endereco = mongoose.model('Endereco', TurmaSchema);

