const mongoose = require('../database');

const AlunoSchema = new mongoose.Schema({

    nome:{
        type:String,
        require:true,
    },
    datadenascimento:{
        type:String,
        require:true,
    }

});

const Aluno = mongoose.model('Aluno', AlunoSchema);

module.exports = Aluno;
