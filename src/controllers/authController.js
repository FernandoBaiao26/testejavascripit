const express = require('express');

const Aluno = require('../models/aluno');

const router = express.Router();

router.post('/cadastrar', async (req,res) => {
    try {
        
        const aluno = await Aluno.create(req.body);
        return res.send({aluno});

    } catch (err) {
        
        return res.status(400).send({error: 'Registration failed'});
    
    }
});

module.exports = app => app.use('/auth', router);