//Importação do express
const express = require('express');

//Importação do body-parser
const bodyParser = require('body-parser');

//Criando um app para pegar o express
const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));


require('./controllers/authController')(app);

app.listen(3000);

